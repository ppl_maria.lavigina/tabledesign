let myViewArray = [
  { charityName: "Earth Trust", distance: 30, total: 23, year: "2008" },
  { charityName: "Animal Trust", distance: 32, total: 54, year: "2005" },
  { charityName: "Childrean Trust", distance: 29, total: 7, year: "2021" },
  { charityName: "Culture Trust", distance: 25, total: 15, year: "2024" },
  { charityName: "Inovation Trust", distance: 27, total: 32, year: "2022" },
  { charityName: "Active Trust", distance: 24, total: 6, year: "2000" },
  { charityName: "Care Trust", distance: 31, total: 8, year: "2007" },
  { charityName: "Education Trust", distance: 2, total: 46, year: "1999" },
  { charityName: "Green Trust", distance: 14, total: 4, year: "2004" },
  { charityName: "Global Trust", distance: 29, total: 65, year: "1998" },
  { charityName: "Support Trust", distance: 85, total: 76, year: "1993" },
  { charityName: "Community Trust", distance: 81, total: 73, year: "1996" },
];

function buildTable(data) {
  var table = document.getElementById("theViewRepo");
  table.innerHTML = "";
  for (var i = 0; i < data.length; i++) {
    let row = `<tr>
    <td>${data[i].charityName}</td>
    <td>${data[i].distance}</td>
    <td>${data[i].total}</td>
    <td>${data[i].year}</td>             
    </tr>`;

    table.innerHTML += row;
  }
}

$("th").on("click", function () {
  console.log("click2");
  let column = $(this).data("colname");
  let order = $(this).data("order");
  let text = $(this).html();
  console.log("column was clicked", column, order);
  text = text.substring(0, text.length - 1);

  if (order == "desc") {
    myViewArray = myViewArray.sort((a, b) => (a[column] > b[column] ? 1 : -1));
    $(this).data("order", "asc");
    text += "&#9660";
  } else {
    myViewArray = myViewArray.sort((a, b) => (a[column] < b[column] ? 1 : -1));
    $(this).data("order", "desc");
    text += "&#9650";
  }

  $(this).html(text);
  buildTable(myViewArray);
});
